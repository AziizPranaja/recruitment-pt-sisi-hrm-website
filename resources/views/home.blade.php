@extends('layouts.master')
@section('title', 'Dashboard')


@section('content')
    <div class="box-breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#" class="d-flex align-items-center"><i class="material-icons">home</i>
                        Beranda</a>
                </li>
            </ol>
        </nav>
    </div>
    <div class="box-content" style="min-height: 70vh">

    </div>
@endsection
