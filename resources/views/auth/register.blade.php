@extends('layouts.lanpage-login')

@section('title', 'REGISTER')

@section('content')
<div class="box-login">
    <div class="content-login">
        <div class="frame-logo-login">
            {{-- <img src="{{ asset('images/internal-images/logo.png') }}" alt="" /> --}}
        </div>
        <h5>Register</h5>
        <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
            @csrf
            <div class="mb-4 mt-3">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Name" autofocus>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mb-4 mt-3">
                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Username" required autocomplete="username" autofocus>
                @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mb-4 mt-3">
                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail1" placeholder="Email" />
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mb-4 mt-3">
                <input id="no_hp" type="number" class="form-control @error('no_hp') is-invalid @enderror" name="no_hp" placeholder="No Hp" value="{{ old('no_hp') }}" required autocomplete="no_hp">
                @error('no_hp')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mb-4 mt-3">
                <input id="wa" type="number" class="form-control @error('wa') is-invalid @enderror" name="wa" placeholder="No Wa" value="{{ old('wa') }}" required autocomplete="wa">
                @error('wa')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mb-4 mt-3">
                <input id="pin" type="number" class="form-control @error('pin') is-invalid @enderror" name="pin" placeholder="Pin" value="{{ old('pin') }}" required autocomplete="pin">
                @error('pin')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mb-3">
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password" />
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mb-4 mt-3">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password" autocomplete="new-password">
            <div class="mb-3 d-flex align-items-center">
            <div class="mb-4 mt-3">
                <input id="photo" type="file" class="form-control" name="photo" required>
                @error('photo')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <div class="mb-3 d-flex align-items-center">
                {{-- <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                <label class="label-cekbok ms-2" for="exampleCheck1">Simpan sandi</label> --}}
                {{-- <a href="{{ url('/lupa-sandi') }}" class="ms-auto lupa-sandi">Lupa sandi?</a> --}}
            </div>
            <button type="submit" class="btn btn-login" id="btn-login">
                Daftar
                <i class="material-icons" id="icon-login">arrow_forward</i>
            </button>
        </form>
    </div>
</div>
@endsection

@push('addon-javascript')
    <script>
        const btnLogin = document.querySelector("#btn-login");
        const iconLLogin = document.querySelector("#icon-login");
        const inputPassword = document.querySelector("#password");
        const iconPassword = document.querySelector("#material-password");

        btnLogin.addEventListener("mouseover", () => {
            iconLLogin.style.display = "block";
        });
        btnLogin.addEventListener("mouseout", () => {
            iconLLogin.style.display = "none";
        });

        iconPassword.addEventListener("click", () => {
            if (inputPassword.type == "password") {
                inputPassword.type = "text";
                iconPassword.innerHTML = "visibility_off";
            } else {
                inputPassword.type = "password";
                iconPassword.innerHTML = "visibility";
            }
        });
    </script>
@endpush
