@extends('layouts.master')
@section('title', 'Data Activity')
@section('meta_header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('content')

    <div class="box-breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#" class="d-flex align-items-center"><i class="material-icons">home</i>
                        Beranda</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <div class="d-flex align-items-center">
                        <img src="{{ asset('images/internal-images/icon-master.png') }}"
                            class="d-flex align-items-center me-1" width="16px" height="16px" alt=""> Data Pegawai
                    </div>
                </li>
            </ol>
        </nav>
    </div>
    <div class="box-content">
        <h5>Data Pegawai</h5>
        <div class="d-md-flex align-items-md-center justify-content-md-between mt-2">
            {{-- <a href="/menu/create-menu" class="btn-create">
                Tambah Data
            </a> --}}
            <div class="form-search">
                <input type="text" name="" id="search" placeholder="Cari" />
            </div>
        </div>
        <div class="outher-table" id="facility-table">
            <div id="list-menu" class="table-scroll">
                <table class="table-master" style="width:100%;">
                    <thead>
                        <tr>
                            <th style="width:10%" class="text-center">Name</th>
                            <th style="width:30%" class="text-center">Username</th>
                            <th style="width:15%" class="text-center">Email</th>
                            <th style="width:15%" class="text-center">NIK</th>
                            <th style="width:15%" class="text-center">Tempat Lahir</th>
                            <th style="width:15%" class="text-center">Tanggal Lahir</th>
                            <th style="width:15%" class="text-center">Jenis Kelamin</th>
                            <th style="width:15%" class="text-center">Alamat</th>
                            <th style="width:15%" class="text-center">Agama</th>
                            <th style="width:15%" class="text-center">Pendidikan Terakhir</th>
                            <th style="width:15%" class="text-center">Jabatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($employee as $new)
                            <tr>
                                <td class="text-center align-items-center" style="width: 10%">{{ $new->name }}</td>
                                <td style="width:30%">{{ $new->username }}</td>
                                <td style="width:15%">{{ $new->email }}</td>
                                <td style="width:15%">{{ $new->nik }}</td>
                                <td style="width:15%">{{ $new->place_of_birth }}</td>
                                <td style="width:15%">{{ $new->date_of_birth }}</td>
                                <td style="width:15%">{{ $new->gender }}</td>
                                <td style="width:15%">{{ $new->address }}</td>
                                <td style="width:15%">{{ $new->religion }}</td>
                                <td style="width:15%">{{ $new->education }}</td>
                                <td style="width:15%">{{ $new->position }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Tidak ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('addon-javascript')
    <script>
        const inpuTSearch = document.querySelector("#search");
        inpuTSearch.addEventListener("keyup", searchDataTable);

        function searchDataTable() {
            let filter, table, tr, td, i, txtValue;
            filter = inpuTSearch.value.toUpperCase();
            table = document.querySelector("#list-menu");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    // console.log(txtValue.toUpperCase().indexOf(filter))
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
    <script>
         $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        function delete_data(e) {
            Swal.fire({
                text: "Apakah anda yakin ingin menghapus ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonColor: '#3085d6',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Setuju',
                reverseButtons: true

            }).then(function (result) {

            if (result.value) {

                var id = e.attr('data-id');
                jQuery.ajax({
                url: "{{url('/menu/delete-menu')}}" + "/" + id,
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    '_method': 'delete'
                },
                success: function (result) {

                    if (result.error) {

                    Swal.fire({
                        type: "error",
                        title: 'Oops...',
                        text: result.message,
                        confirmButtonClass: 'btn btn-success',
                    })

                    } else {

                        setTimeout(() => {
                                $("#list-menu").load(window.location.href +
                                    " #list-menu");
                            }, 0);

                            setTimeout(() => {
                                $(".sidebar").load(window.location.href +
                                    " .sidebar");
                            }, 0);

                    Swal.fire({
                        type: "success",
                        title: 'Menghapus!',
                        text: result.message,
                        confirmButtonClass: 'btn btn-success',
                    })

                    }
                }
                });
            }
            });
        }
    </script>
@endpush
