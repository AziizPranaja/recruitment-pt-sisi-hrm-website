@extends('layouts.dashboard-layouts')

@section('title', 'Formulir Pegawai')

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
@endsection

@section('content')
    <div class="container">
    </div>

    <section class="formulir">
        <div class="container">
            <div class="box-formulir">
                <div class="text-formulir text-center">
                    <div class="header-text">Data Pegawai</div>
                    <div class="biodata-text-form">BIODATA PEGAWAI</div>
                </div>
                <form action="{{ route('formemployee') }}" method="post">
                    @csrf
                    <div class="box-form-input">
                        <div class="mb-3">
                            <label for="nik" class="form-label">NIK</label>
                            <input type="number" name="nik" class="form-control" id="nik"
                                placeholder="NUPTK / ID Pegawai" />
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Tempat, Tanggal Lahir</label>
                            <div class="row">
                                <div class="col-6">
                                    <input type="text" name="place_of_birth" class="form-control" id="tempatLahir"
                                        placeholder="Tempat Lahir" />
                                </div>
                                <div class="col-6">
                                    <input type="text" name="date_of_birth" class="form-control bg-calendar"
                                        id="tglLahir" placeholder="dd-mm-yy" />
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Jenis Kelamin</label>
                            <select class="form-select" id="gender" name="gender" aria-label="Default select example" data-dropdown-parent="body" data-placeholder="--- Pilih Jenis Kelamin ---">
                                <option></option>
                                <option value="Laki-Laki">Laki-Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Agama</label>
                            <select class="form-select" id="religion" name="religion" aria-label="Default select example" data-dropdown-parent="body" data-placeholder="--- Pilih Agama ---">
                                <option></option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                                <option value="Konghucu">Konghucu</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Alamat Tinggal</label>
                            <textarea class="form-control" name="address" id="exampleFormControlTextarea1" rows="3"
                                placeholder="Alamat Lengkap"></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Riwayat Pendidikan</label>
                            <input type="text" name="education" class="form-control" id="asalSekolah"
                                placeholder="Riwayat Pendidikan" />
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Jabatan</label>
                            <input type="text" name="position" class="form-control" id="nisn"
                                placeholder="Jabatan" />
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Shift Kerja</label>
                            <select class="form-select" name="workshift_id" id="workshift_id" aria-label="Default select example" data-dropdown-parent="body" data-placeholder="--- Pilih Shift Kerja ---">
                                <option></option>
                                @foreach ($workshift as $e)
                                <option value="{{ $e->workshift_id }}">{{ $e->shift_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button class="submit-form-siswa" type="submit">Kirim</button>
                </form>
            </div>
        </div>
    </section>
@endsection

@push('addon-javascript')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        function uploadPhotoSiswa() {
            document.querySelector("#fotoSiswa").click();
        }

        $('#workshift_id').select2({
            theme: "bootstrap-5",
            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            placeholder: $(this).data('placeholder'),
            dropdownParent: $('#exampleModal'),
        });

        $('#gender').select2({
            theme: "bootstrap-5",
            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            placeholder: $(this).data('placeholder'),
            dropdownParent: $('#exampleModal'),
        });

        $('#religion').select2({
            theme: "bootstrap-5",
            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            placeholder: $(this).data('placeholder'),
            dropdownParent: $('#exampleModal'),
        });

        flatpickr("#tglLahir", {
            altInput: true,
            altFormat: "j F, Y",
            dateFormat: "d-m-Y",
        });

        const inputImage = document.querySelector("#fotoSiswa");
        const text = document.querySelector("#text-preview");
        inputImage.addEventListener("change", () => {
            let reader = new FileReader();
            reader.readAsDataURL(inputImage.files[0]);
            text.textContent = inputImage.files[0].name;
            console.log(inputImage.files[0].name);
            // reader.onload = () => {
            //     choseImage.setAttribute("src", reader.result);
            // }
        });
    </script>
@endpush
