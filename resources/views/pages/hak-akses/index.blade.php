@extends('layouts.master')
@section('title', 'Data Hak Akses')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Hak Akses</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif

                        <form action="{{ route('menu.access.add') }}" method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="id_user">Pilih User:</label>
                                <select name="id_user" id="id_user" class="form-control">
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id_user }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="menu_id">Pilih Menu:</label>
                                <select name="menu_id" id="menu_id" class="form-control">
                                    @foreach ($menus as $menu)
                                        <option value="{{ $menu->menu_id }}">{{ $menu->menu_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>

                        <hr>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>User</th>
                                    <th>Hak Akses</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>
                                            @foreach ($user->menus as $menu)
                                                {{ $menu->menu_name }}
                                                @if (!$loop->last)
                                                    ,
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <form action="{{ route('menu.access.destroy') }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <input type="hidden" name="user_id" value="{{ $user->id_user }}">
                                                <div class="form-group">
                                                    <select name="menu_id" class="form-control">
                                                        @foreach ($user->menus as $menu)
                                                            <option value="{{ $menu->menu_id }}">{{ $menu->menu_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
