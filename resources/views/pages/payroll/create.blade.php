@extends('layouts.dashboard-layouts')

@section('title', 'Create Menu')

@section('content')
    <div class="container">
        <div class="box-breadcrumb">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/home" class="d-flex align-items-center"><i class="material-icons">home</i> Beranda</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        Data Penggajian
                    </li>
                </ol>
            </nav>
        </div>
    </div>

    <section class="new-news">
        <div class="container">
            <div class="box-news">
                <div class="title-news">Input Penggajian</div>
                <div class="box-form">
                    <form action="{{ route('payroll.post') }}" method="POST">
                        @csrf
                        <div class="row mb-3">
                            <div class="col-md-6 col-12">
                                <label for="employee_id" class="form-label">Pegawai</label>
                                <select class="form-select" name="employee_id" id="employee_id">
                                    <option selected>--- Pilih Pegawai ---</option>
                                    @foreach ($employees as $emp)
                                    <option value={{ $emp->employee_id }}>{{ $emp->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 col-12">
                                <div class="mb-3">
                                    <label for="basic_salary" class="form-label">Gaji Pokok</label>
                                    <input type="number" name="basic_salary" class="form-control" id="basic_salary"
                                        aria-describedby="titleHelp" />
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 col-12">
                                <div class="mb-3">
                                    <label for="allowance" class="form-label">Tunjangan</label>
                                    <input type="number" name="allowance" class="form-control" id="allowance"
                                        aria-describedby="titleHelp" />
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 col-12">
                                <div class="mb-3">
                                    <label for="overtime" class="form-label">Lembur</label>
                                    <input type="number" name="overtime" class="form-control" id="overtime"
                                        aria-describedby="titleHelp" />
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 col-12">
                                <div class="mb-3">
                                    <label for="deduction" class="form-label">Potongan</label>
                                    <input type="number" name="deduction" class="form-control" id="deduction"
                                        aria-describedby="titleHelp" />
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 col-12">
                                <div class="mb-3">
                                    <label for="payroll_date" class="form-label">Tanggal Penggajian</label>
                                    <input type="text" name="payroll_date" class="form-control bg-calendar"
                                        id="payroll_date" placeholder="dd-mm-yy" />
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="d-md-flex d-block justify-content-end">
                                    <button type="reset" class="btn-news red">Batal</button>
                                    <button type="submit" class="btn-news blue">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('addon-javascript')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.ckeditor.com/4.20.0/standard/ckeditor.js"></script>
    <script>
        function thisUploadImage() {
            document.querySelector("#fileId").click();
        }

        flatpickr("#payroll_date", {
            altInput: true,
            altFormat: "j F, Y",
            dateFormat: "d-m-Y",
        });

        CKEDITOR.replace("text-news");

        const inputImage = document.querySelector("#fileId");
        const choseImage = document.querySelector("#image-upload-btn");

        inputImage.addEventListener("change", () => {
            let reader = new FileReader();
            reader.readAsDataURL(inputImage.files[0]);
            reader.onload = () => {
                choseImage.setAttribute("src", reader.result);
            }
        });
    </script>
@endpush
