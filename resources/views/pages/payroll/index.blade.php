@extends('layouts.master')
@section('title', 'Data Payroll')
@section('meta_header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('content')

    <div class="box-breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#" class="d-flex align-items-center"><i class="material-icons">home</i>
                        Beranda</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <div class="d-flex align-items-center">
                        <img src="{{ asset('images/internal-images/icon-master.png') }}"
                            class="d-flex align-items-center me-1" width="16px" height="16px" alt=""> Data Payroll
                    </div>
                </li>
            </ol>
        </nav>
    </div>
    <div class="box-content">
        <h5>Data Payroll</h5>
        <div class="d-md-flex align-items-md-center justify-content-md-between mt-2">
            <a href="/payroll/create-payroll" class="btn-create">
                Tambah Data
            </a>
            <div class="form-search">
                <input type="text" name="" id="search" placeholder="Cari" />
            </div>

        </div>
        <div class="outher-table" id="facility-table">
            <div id="list-menu" class="table-scroll">
                <table class="table-master" style="width:100%;">
                    <thead>
                        <tr>
                            <th style="width:10%" class="text-center">Nama Penginput</th>
                            <th style="width:30%" class="text-center">Nama Pegawai</th>
                            <th style="width:15%" class="text-center">Gaji Pokok</th>
                            <th style="width:15%" class="text-center">Tunjangan</th>
                            <th style="width:15%" class="text-center">Lembur</th>
                            <th style="width:15%" class="text-center">Potongan</th>
                            <th style="width:15%" class="text-center">Total Gaji</th>
                            <th style="width:15%" class="text-center">Tanggal Penggajian</th>
                            <th style="width:15%" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($payrolls as $new)
                            <tr>
                                <td class="text-center align-items-center" style="width: 10%">{{ $new->name }}</td>
                                <td style="width:30%">{{ $new->username }}</td>
                                <td style="width:15%">Rp.{{ $new->basic_salary }} </td>
                                <td style="width:15%">Rp.{{ $new->allowance }}</td>
                                <td style="width:15%">Rp.{{ $new->overtime }}</td>
                                <td style="width:15%">Rp.{{ $new->deduction }}</td>
                                <td style="width:15%">Rp.{{ $new->total_salary }}</td>
                                <td style="width:15%">{{ $new->payroll_date }}</td>
                                <td style="width: 25%">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <a class="btn-edit-master me-2" href="{{ url('payroll/update-payroll/'.$new->id) }}"><i class="fa fa-edit text-primary"></i></a>
                                        <a data-id="{{ $new->id }}" onclick=delete_data($(this))
                                            class="btn-edit-master">
                                            <i class="fa fa-trash-o text-danger"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Tidak ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('addon-javascript')
    <script>
        const inpuTSearch = document.querySelector("#search");
        inpuTSearch.addEventListener("keyup", searchDataTable);

        function searchDataTable() {
            let filter, table, tr, td, i, txtValue;
            filter = inpuTSearch.value.toUpperCase();
            table = document.querySelector("#list-menu");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    // console.log(txtValue.toUpperCase().indexOf(filter))
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
    <script>
         $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        function delete_data(e) {
            Swal.fire({
                text: "Apakah anda yakin ingin menghapus ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonColor: '#3085d6',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Setuju',
                reverseButtons: true

            }).then(function (result) {

            if (result.value) {

                var id = e.attr('data-id');
                jQuery.ajax({
                url: "{{url('/payroll/delete-payroll')}}" + "/" + id,
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    '_method': 'delete'
                },
                success: function (result) {

                    if (result.error) {

                    Swal.fire({
                        type: "error",
                        title: 'Oops...',
                        text: result.message,
                        confirmButtonClass: 'btn btn-success',
                    })

                    } else {

                        setTimeout(() => {
                                $("#list-menu").load(window.location.href +
                                    " #list-menu");
                            }, 0);

                    Swal.fire({
                        type: "success",
                        title: 'Menghapus!',
                        text: result.message,
                        confirmButtonClass: 'btn btn-success',
                    })

                    }
                }
                });
            }
            });
        }
    </script>
@endpush
