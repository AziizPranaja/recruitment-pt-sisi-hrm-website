<h5>Permohonan Izin / Cuti</h5>
<form action="{{ url('absensi/cuti') }}" method="post">
    @csrf
    <div class="mb-3">
        <select class="form-select" name="leave_type_id" id="select-2-field" data-placeholder="Jenis Cuti">
            <option></option>
            @foreach ($leaveType as $e)
            <option value="{{ $e->leave_type_id }}">{{ $e->leave_type_name }}</option>
            @endforeach
        </select>

    </div>
    <div class="mb-3">
        <label for="">Nama Lengkap</label>
        <input type="text" class="form-control" disabled
            value="{{ $user->name }}" />
    </div>
    <div class="mb-3">
        <label for="">Tanggal Mulai</label>
        <input type="text" name="application_from_date" id="application_from_date" placeholder="dd-mm-yy"
            class="form-control bg-calendar" />
    </div>
    <div class="mb-3">
        <label for="">Tanggal Berakhir</label>
        <input type="text" name="application_to_date" id="application_to_date" placeholder="dd-mm-yy"
            class="form-control bg-calendar" />
    </div>
    <div class="mb-3">
        <label for="">Keterangan Cuti</label>
        <input type="text" name="purpose" id="" class="form-control" placeholder="Keterangan Cuti" />
    </div>
    <div class="mb-3">
        <label for="">Pekerjaan Yang Ditinggalkan</label>
        <input type="text" name="abandoned_job" id="" class="form-control"
            placeholder="Pekerjaan Yang Ditinggalkan" />
    </div>
    <div class="d-flex justify-content-center">
        <button type="submit" class="btn-submit">Kirim</button>
    </div>
</form>
