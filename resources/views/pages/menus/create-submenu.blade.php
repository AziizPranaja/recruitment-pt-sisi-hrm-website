@extends('layouts.dashboard-layouts')

@section('title', 'Create Submenu')

@section('content')
    <div class="container">
        <div class="box-breadcrumb">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/home" class="d-flex align-items-center"><i class="material-icons">home</i> Beranda</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        Data Menu
                    </li>
                </ol>
            </nav>
        </div>
    </div>

    <section class="new-news">
        <div class="container">
            <div class="box-news">
                <div class="title-news">Input Submenu</div>
                <div class="box-form">
                    <form action="/menu/{{ $parentMenu->menu_id }}/add-submenu" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <div class="col-md-6 col-12">
                                <div class="mb-3">
                                    <label for="menu_name" class="form-label">Nama Submenu</label>
                                    <input type="text" name="menu_name" class="form-control" id="menu_name"
                                        aria-describedby="titleHelp" />
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 col-12">
                                <div class="mb-3">
                                    <label for="menu_link" class="form-label">Link Submenu</label>
                                    <input type="text" name="menu_link" class="form-control" id="menu_link"
                                        aria-describedby="titleHelp" />
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 col-12">
                                <div class="mb-3">
                                    <label for="menu_icon" class="form-label">Icon Submenu</label>
                                    <input type="text" name="menu_icon" class="form-control" id="menu_icon"
                                        aria-describedby="titleHelp" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <label for="id_level" class="form-label">Level</label>
                            <select class="form-select" name="id_level" id="id_level">
                                <option selected>--- Pilih Level ---</option>
                                @foreach ($levels as $level)
                                <option value={{ $level->id_level }}>{{ $level->level }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="d-md-flex d-block justify-content-end">
                                    <button type="reset" class="btn-news red">Batal</button>
                                    <button type="submit" class="btn-news blue">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('addon-javascript')
    <script src="https://cdn.ckeditor.com/4.20.0/standard/ckeditor.js"></script>
    <script>
        function thisUploadImage() {
            document.querySelector("#fileId").click();
        }

        CKEDITOR.replace("text-news");

        const inputImage = document.querySelector("#fileId");
        const choseImage = document.querySelector("#image-upload-btn");

        inputImage.addEventListener("change", () => {
            let reader = new FileReader();
            reader.readAsDataURL(inputImage.files[0]);
            reader.onload = () => {
                choseImage.setAttribute("src", reader.result);
            }
        });
    </script>
@endpush
