@extends('layouts.master')
@section('title', 'Data Menu')
@section('meta_header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('content')

    <div class="box-breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#" class="d-flex align-items-center"><i class="material-icons">home</i>
                        Beranda</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <div class="d-flex align-items-center">
                        <img src="{{ asset('images/internal-images/icon-master.png') }}"
                            class="d-flex align-items-center me-1" width="16px" height="16px" alt=""> Data Menu
                    </div>
                </li>
            </ol>
        </nav>
    </div>
    <div class="box-content">
        <h5>Data Menu</h5>
        <div class="d-md-flex align-items-md-center justify-content-md-between mt-2">
            <a href="/menu/create-menu" class="btn-create">
                Tambah Data
            </a>
            <div class="form-search">
                <input type="text" name="" id="search" placeholder="Cari" />
            </div>

        </div>
        <div class="outher-table" id="facility-table">
            <div id="list-menu" class="table-scroll">
                <table class="table-master" style="width:100%;">
                    <thead>
                        <tr>
                            <th style="width:10%" class="text-center">Id Level</th>
                            <th style="width:30%" class="text-center">Menu Name</th>
                            <th style="width:15%" class="text-center">Menu Link</th>
                            <th style="width:15%" class="text-center">Menu Icon</th>
                            <th style="width:15%" class="text-center">Parent Id</th>
                            <th style="width:15%" class="text-center">Create By</th>
                            <th style="width:15%" class="text-center">Update By</th>
                            <th style="width:15%" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($menuall as $new)
                            <tr>
                                <td class="text-center align-items-center" style="width: 10%">{{ $new->id_level }}</td>
                                <td style="width:30%">{{ $new->menu_name }}</td>
                                <td style="width:15%">{{ $new->menu_link }} </td>
                                <td style="width:15%">{{ $new->menu_icon }}</td>
                                <td style="width:15%">{{ $new->parent_id }}</td>
                                <td style="width:15%">{{ $new->create_by }}</td>
                                <td style="width:15%">{{ $new->update_by }}</td>
                                <td style="width: 25%">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <a class="btn-edit-master me-2" href="{{ url('menu/update-menu/'.$new->menu_id) }}"><i class="fa fa-edit text-primary"></i></a>
                                        <a class="btn-edit-master me-2" href="{{ url('menu/'.$new->menu_id.'/add-submenu') }}"><i class="fa fa-plus text-primary"></i></a>
                                        <a data-id="{{ $new->menu_id }}" onclick=delete_data($(this))
                                            class="btn-edit-master">
                                            <i class="fa fa-trash-o text-danger"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Tidak ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('addon-javascript')
    <script>
        const inpuTSearch = document.querySelector("#search");
        inpuTSearch.addEventListener("keyup", searchDataTable);

        function searchDataTable() {
            let filter, table, tr, td, i, txtValue;
            filter = inpuTSearch.value.toUpperCase();
            table = document.querySelector("#list-menu");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    // console.log(txtValue.toUpperCase().indexOf(filter))
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
    <script>
         $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        function delete_data(e) {
            Swal.fire({
                text: "Apakah anda yakin ingin menghapus ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonColor: '#3085d6',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Setuju',
                reverseButtons: true

            }).then(function (result) {

            if (result.value) {

                var id = e.attr('data-id');
                jQuery.ajax({
                url: "{{url('/menu/delete-menu')}}" + "/" + id,
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    '_method': 'delete'
                },
                success: function (result) {

                    if (result.error) {

                    Swal.fire({
                        type: "error",
                        title: 'Oops...',
                        text: result.message,
                        confirmButtonClass: 'btn btn-success',
                    })

                    } else {

                        setTimeout(() => {
                                $("#list-menu").load(window.location.href +
                                    " #list-menu");
                            }, 0);

                            setTimeout(() => {
                                $(".sidebar").load(window.location.href +
                                    " .sidebar");
                            }, 0);

                    Swal.fire({
                        type: "success",
                        title: 'Menghapus!',
                        text: result.message,
                        confirmButtonClass: 'btn btn-success',
                    })

                    }
                }
                });
            }
            });
        }
    </script>
@endpush
