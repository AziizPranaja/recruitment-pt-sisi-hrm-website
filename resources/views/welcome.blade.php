@extends('layouts.landing')

@section('title', 'LANDING PAGE')

@section('content')
    <section class="header-landing" id="home">
        <div class="bg-landing-page"></div>
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"><i class="material-icons">view_headline</i></span>
                </button>
            </div>
        </nav>

        <div class="container">
            <div class="content-landing">
                <div class="page-text">
                    <h3>Selamat Datang</h3>
                    <h1>Bonjour</h1>
                    <div class="description">
                        <p>
                            Website ini merupakan platform pengelolaan sumber daya manusia yang lengkap dan terintegrasi, membantu perusahaan dalam mengelola perekrutan, pengembangan karyawan, pengelolaan gaji dan tunjangan, serta memantau kinerja karyawan secara efektif dan efisien.
                        </p>
                    </div>
                    <a href="{{ route('login') }}" class="btn btn-mulai">Log in</a>
                    <a href="{{ route('register') }}" class="btn btn-mulai">Register</a>
                </div>
                <div class="page-image">
                    <div class="frame-image">
                        <img src="{{ asset('images/internal-images/img-landing-page.png') }}" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer id="about-me">
        <div class="container">
            <div class="bottom-footer">
                <div class="row text-center">
                    <div class="col-12 text-bottom-footer">
                        &copy
                        <script>
                            document.write(new Date().getFullYear());
                        </script>
                        All Rights Reserved
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="top">
        <a href="#home"><i class="material-icons">expand_less</i></a>
    </div>
@endsection

@push('addon-javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".news-new").slick({
                dots: true,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 2,
                autoplay: true,
                arrows: false,
                speed: 300,
                centerPadding: "100px",
                autoplaySpedd: 0.1,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: true,
                        },
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                        },
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                        },
                    },
                ],
            });
        });
        $(".hot-news").slick({
            autoplay: true,
            arrows: false,
            dots: true,
        });

        $(".center").slick({
            centerMode: true,
            centerPadding: "100px",
            slidesToShow: 3,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: "40px",
                        slidesToShow: 3,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: "40px",
                        slidesToShow: 1,
                    },
                },
            ],
        });
    </script>
    <script>
        const toTop = document.querySelector(".top");

        window.addEventListener("scroll", () => {
            if (window.pageYOffset > 100) {
                toTop.classList.add("active");
            } else {
                toTop.classList.remove("active");
            }
        });
    </script>
@endpush

