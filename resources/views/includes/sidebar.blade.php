  <div class="sidebar">
      <div class="logo-details">
          <div class="box-logo">
              {{-- <img src="{{ asset('images/internal-images/logo.png') }}" alt="" /> --}}
          </div>
          <div class="logo-name">
              {{-- <h6>SIPENLA</h6> --}}
              {{-- <div class="text-logo-name">Data Master</div> --}}
          </div>
          <i class="fa fa-angle-double-right" id="btn-icon"></i>
      </div>
      <ul class="nav-list">
        @foreach(auth()->user()->menus as $menu)
            <li>
                <a id="dropdown-keuangan">
                    <div class="box-icon">
                        <i class="{{ $menu->menu_icon }}"></i>
                    </div>
                    <span class="link-name">{{ $menu->menu_name }}</span>
                    <div class="arrow ms-auto">
                        <i class="fa fa-angle-down"></i>
                    </div>
                </a>
                <span class="tool">{{ $menu->menu_name }}</span>
                @if($menu->children->count())
                    <ul class="menu-dropdown">
                        @foreach($menu->children as $childMenu)
                            <li>
                                <a href="{{ $childMenu->menu_link }}">
                                    <div class="box-icon-dropdown">
                                        <i class="{{ $childMenu->menu_icon }}"></i>
                                    </div>
                                    <div class="text-dropdown">{{ $childMenu->menu_name }}</div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
      </ul>
  </div>
