 <nav class="navbar navbar-expand-lg sticky-top">
     <div class="container">
         <div class="d-flex d-lg-none align-items-center">
             <div class="dropdown">
                 <button class="btn btn-dropdown" href="#" role="button" data-bs-toggle="dropdown"
                     aria-expanded="false">
                     <i class="material-icons">notifications_none</i>
                 </button>
                 <ul class="dropdown-menu">
                     <li><a class="dropdown-item" href="#">Action</a></li>
                     <li>
                         <hr class="dropdown-divider" />
                     </li>
                     <li>
                         <a class="dropdown-item" href="">test</a>
                     </li>
                 </ul>
             </div>
             <div class="dropstart">
                 <button class="btn btn-dropdown" href="#" role="button" data-bs-toggle="dropdown"
                     aria-expanded="false">
                     <img src="{{ asset('images/internal-images/user.png') }}" style="border-radius: 50%" alt=""
                         width="35" height="35" />
                 </button>
                 <ul class="dropdown-menu mt-5">
                     <li><a class="dropdown-item" href="#">Action</a></li>
                     <li>
                         <hr class="dropdown-divider" />
                     </li>
                     <li>
                         <a class="dropdown-item" href="">test</a>
                     </li>
                 </ul>
             </div>
         </div>
         <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
             aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"><i class="material-icons">view_headline</i></span>
         </button>
         <div class="d-lg-flex d-none ms-auto align-items-center">
             <div class="dropstart">
                 <button class="btn btn-dropdown" href="#" role="button" data-bs-toggle="dropdown"
                     aria-expanded="false">
                     <img src="{{ asset('images/internal-images/user.png') }}" style="border-radius: 50%"
                         alt="" width="35" height="35" />
                 </button>
                 <ul class="dropdown-menu mt-5">
                     <li>
                         <a class="dropdown-item" href="{{ url('/logout') }}">Logout</a>
                     </li>
                 </ul>
             </div>
         </div>
     </div>
 </nav>
