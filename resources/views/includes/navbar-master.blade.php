<nav class="navbar navbar-expand-lg">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ms-auto d-flex align-items-center">
                <li class="nav-item dropdown">
                    <div class="dropstart">
                        <a class="nav-link" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <div class="box-icon-user">
                                <img src="{{ asset('images/internal-images/user.png') }}" alt="" />
                            </div>
                        </a>
                        <ul class="dropdown-menu mt-5">
                            <li>
                                <a class="dropdown-item" href="{{ url('/logout') }}">Logout</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
