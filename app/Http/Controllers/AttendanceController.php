<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Attendance;
use App\Models\LeaveApplication;
use App\Models\OfficialDuty;
use App\Models\Workday;
use App\Models\LeaveType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Url;
use Illuminate\Support\Str;


class AttendanceController extends Controller
{

    public function saveImage($image, $path = 'public')
    {
        if (!$image) {
            return null;
        }

        $filename = time() . '.png';
        // save image
        Storage::disk($path)->put($filename, base64_decode($image));

        //return the path
        // Url is the base url exp: localhost:8000
        return URL::to('/') . '/storage/' . $path . '/' . $filename;
    }

    public function index()
    {
        return view('pages.absensi.absensi-webcam');
    }

    public function absensi()
    {
        $user = Auth::user();
        $attendance = Attendance::where('id_user', $user->id_user)->whereNull('check_out')->where('date', date("Y-m-d"))->first();
        $leaveType = LeaveType::all();

        $byweek = Attendance::where('id_user', $user->id_user)
                    ->get()
                    ->groupBy(function($date) {
                        return Carbon::parse($date->date)->format('W');
                    });

        $byweek = $byweek->reverse();

        $leave = LeaveType::get();

        return view('pages.absensi.absensi', compact('user', 'attendance', 'byweek', 'leave', 'leaveType'));
    }

    public function absensiKeluar()
    {
        $user = Auth::user();
        $attendance = Attendance::where('id_user', $user->id_user)->where('date', date("Y-m-d"))->first();

        return view('pages.absensi.absensi-keluar', compact('attendance'));
    }

    public function checkin(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $employee = Employee::where('id_user', '=', $user->id_user)->first();
        $date = Carbon::now();
        $day = Carbon::parse($date);
        $day->settings(['formatFunction' => 'translatedFormat']);

        $leave = LeaveApplication::where('id_user', '=', $user->id_user)
                -> whereDate('application_from_date', '<=', $date)
                -> whereDate('application_to_date', '>=', $date)
                ->get();

        $duty = OfficialDuty::where('id_user', '=', $user->id_user)
                -> whereDate('duty_from_date', '<=', $date)
                -> whereDate('duty_to_date', '>=', $date)
                ->get();

        $hasCheckedIn = Attendance::whereDate('date', $date)
                        ->where('id_user', '=', $user->id_user)
                        ->exists();

        $notWorkingDay = Workday::join('days', 'workdays.day_id', '=', 'days.day_id')
                        ->join('workshifts', 'workdays.workshift_id', '=', 'workshifts.workshift_id')
                        ->where('days.day_name', '=', $day->format('l'))
                        ->where('workdays.company_id', '=', $employee->company_id)
                        ->where('workdays.workshift_id', '=', $employee->workshift_id)
                        ->get();

        if (count($leave) > 0) {
            return response()->json([
                'error' => "Anda sedang dalam periode cuti/izin"
            ]);
        }

        if (count($duty) > 0) {
            return response()->json([
                'error' => "Anda sedang dalam periode Tugas Dinas"
            ]);
        }

        if (count($notWorkingDay) <= 0) {
            return response()->json([
                'error' => "Hari ini bukan hari kerja"
            ]);
        }

        foreach($notWorkingDay as $work) {
            if($date->format('H:i:s') > $work->end_time) {
                return response()->json([
                    'error' => "Jam Kerja Telah Usai"
                ]);
            } elseif($date->format('H:i:s') < $work->start_time) {
                return response()->json([
                    'error' => "Jam Kerja Belum Mulai"
                ]);
            }
        }

        if($hasCheckedIn) {
            return response()->json([
                'error' => "Anda sudah melakukan absensi masuk hari ini"
            ]);
        }

        $image = $request->check_in;
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageFix = $this->saveImage($image, "attendances_check_in");

        $checkIn = Attendance::create([
            'uuid' => Str::uuid(),
            'id_user' => $user->id_user,
            'date' => $date,
            'check_in' => $date,
            'image_check_in' => $imageFix,
        ]);

        return redirect('/absensi/landpage');
    }

    public function checkOut(Request $request)
    {
        $data = $request->all();
        $timeNow = Carbon::now();
        $user = Auth::user();
        $employee = Employee::where('id_user', '=', $user->id_user)->first();

        $image = $request->check_out;
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageFix = $this->saveImage($image, "attendances_check_out");

        $hasCheckedIn = Attendance::whereDate('date', $timeNow)
                            ->where('id_user', '=', $user->id_user)
                            ->exists();

        $notCheckOut = Attendance::where('id_user', '=', $user->id_user)
                        ->whereDate('date', $timeNow);

        if(!is_null($notCheckOut->first()->check_out)){
            return response()->json([
                'error' => "Anda sudah melakukan absensi keluar hari ini"
            ]);
        }

        if(!$hasCheckedIn){
            return response()->json([
                'error' => "Anda harus melakukan absensi masuk hari ini"
            ]);
        }

        $checkIn = Attendance::where('uuid', '=', $request->attendance_id)->update([
            'check_out' => $timeNow,
            'image_check_out' => $imageFix,
            'status' => 'ace'
        ]);

        return redirect('/absensi/landpage');
    }

    public function addLeave(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();

        $validate = Validator::make($data, [
            'leave_type_id' => 'required',
            'application_from_date' => 'required',
            'application_to_date' => 'required|after:application_from_date',
            'purpose' => 'required',
            'abandoned_job' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'error' => $validate->errors()->toArray()
            ]);
        }

        $leaveData = LeaveApplication::create([
            'id_user' => $user->id_user,
            'leave_type_id' => $data['leave_type_id'],
            'application_from_date' => $data['application_from_date'],
            'application_to_date' => $data['application_to_date'],
            'application_date' => Carbon::now(),
            'purpose' => $data['purpose'],
            'abandoned_job' => $data['abandoned_job'],
        ]);

        return back();
    }

    public function addDuty(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();

        $validate = Validator::make($data, [
            'duty_from_date' => 'required|date_format:Y-m-d',
            'duty_to_date' => 'required|date_format:Y-m-d|after:duty_from_date',
            'purpose' => 'required',
            'attachment' => 'required',
            'time' => 'required',
            'place' => 'required',
            'abandoned_job' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'error' => $validate->errors()->toArray()
            ]);
        }

        if ($request->hasFile('attachment')) {
            $attachment = $request->file('attachment')->store('attachment');
        } else {
            $attachment = '';
        }

        $leaveData = OfficialDuty::create([
            'id_user' => $user->id_user,
            'duty_from_date' => Carbon::createFromFormat('Y-m-d', $data['duty_from_date']),
            'duty_to_date' => Carbon::createFromFormat('Y-m-d', $data['duty_to_date']),
            'duty_date' => Carbon::now(),
            'purpose' => $data['purpose'],
            'time' => Carbon::parse($data['time'])->format('H:i:s'),
            'place' => $data['place'],
            'abandoned_job' => $data['abandoned_job'],
            'attachment' => $attachment
        ]);

        return back();
    }
}
