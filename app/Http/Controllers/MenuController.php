<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\MenuLevel;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\UserActivity;


class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::whereNull('parent_id')->orderBy('created_at')->get();
        $menuall = Menu::all();
        // $menu = Menu::where('menu_name', 'Menu')->first();

        // UserActivity::log('Mengakses Route /menu', 'success', $menu->menu_id);

        return view('pages.menus.index', compact('menus', 'menuall'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $levels = MenuLevel::all();
        return view('pages.menus.create', compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();

        $validate = Validator::make($data, [
            'id_level' => 'required',
            'menu_name' => 'required',
            'menu_link' => 'required',
            'menu_icon' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'error' => $validate->errors()->toArray()
            ]);
        }

        $menu = Menu::create([
            'id_level' => $data['id_level'],
            'menu_name' => $data['menu_name'],
            'menu_link' => $data['menu_link'],
            'menu_icon' => $data['menu_icon'],
            'create_by' => $user->name,
            'update_by' => $user->name,
        ]);

        return redirect('/menu');
    }

    public function createSubmenu($id)
    {
        $parentMenu = Menu::findOrFail($id);
        $levels = MenuLevel::all();

        return view('pages.menus.create-submenu', compact('parentMenu', 'levels'));
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeSubmenu(Request $request, $id)
    {
        $parentMenu = Menu::findOrFail($id);
        $user = Auth::user();

        $submenu = new Menu();
        $submenu->menu_name = $request->menu_name;
        $submenu->id_level = $request->id_level;
        $submenu->menu_link = $request->menu_link;
        $submenu->menu_icon = $request->menu_icon;
        $submenu->parent_id = $parentMenu->menu_id;
        $submenu->create_by = $user->name;
        $submenu->update_by = $user->name;

        $parentMenu->children()->save($submenu);

        return redirect('/menu');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::where('menu_id', '=', $id)->join('menu_levels', 'menus.id_level', '=', 'menu_levels.id_level')->first();
        $levels = MenuLevel::all();
        return view('pages.menus.update', compact('menu', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $edit = [
            "id_level" => $request->id_level,
            "menu_name" => $request->menu_name,
            "menu_link" => $request->menu_link,
            "menu_icon" => $request->menu_icon,
            "update_by" => $user->name,
            "updated_at" => Carbon::now(),
        ];

        $updateMenu = Menu::where('menu_id', '=', $id)
                        ->update($edit);

        return redirect('/menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $menu = Menu::findOrFail($id);
            $menu->delete();
        } catch (Exception $e) {

            return response()->json(["error" => true, "message" => $e->getMessage()]);
        }

        return response()->json(["error" => false, "message" => "Sukses Menghapus Data Menu!"]);
    }
}
