<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ErrorApplication;
use App\Models\Menu;
use App\Models\UserActivity;

class ErrorLogController extends Controller
{
    public function index()
    {
        $logs = ErrorApplication::join('users', 'error_application.id_user', '=', 'users.id_user')->get();
        $menu = Menu::where('menu_name', 'Error Log')->first();

        UserActivity::log('Mengakses Route /errorlog', 'success', $menu->menu_id);

        return view('pages.error-log.index', compact('logs'));
    }
}
