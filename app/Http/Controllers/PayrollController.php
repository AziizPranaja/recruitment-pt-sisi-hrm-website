<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Payroll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PayrollController extends Controller
{
    public function index()
    {
        $payrolls = Payroll::join('users', 'payrolls.id_user', '=', 'users.id_user')->orderByDesc('payroll_date')->get();

        return view('Pages.payroll.index', compact('payrolls'));
    }

    public function create()
    {
        $employees = Employee::join('users', 'employees.id_user', '=', 'users.id_user')->get();

        return view('pages.payroll.create', compact('employees'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();

        $validate = Validator::make($data, [
            'payroll_date' => 'required',
            'employee_id' => 'required',
            'basic_salary' => 'required|numeric',
            'allowance' => 'required|numeric',
            'overtime' => 'required|numeric',
            'deduction' => 'required|numeric',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'error' => $validate->errors()->toArray()
            ]);
        }

        Payroll::create([
            'id_user' => $user->id_user,
            'payroll_date' => Carbon::parse($data['payroll_date'])->format('Y-m-d'),
            'employee_id' => $data['employee_id'],
            'basic_salary' => $data['basic_salary'],
            'allowance' => $data['allowance'],
            'overtime' => $data['overtime'],
            'deduction' => $data['deduction'],
            'total_salary' => $data['basic_salary'] + $data['allowance'] +  $data['overtime'] - $data['deduction'],
        ]);

        return redirect()->route('payroll.index');
    }

    public function edit($id)
    {
        $payroll = Payroll::where('id', '=', $id)->join('employees', 'payrolls.employee_id', '=', 'employees.employee_id')->join('users', 'payrolls.id_user', '=', 'users.id_user')->first();
        $employees = Employee::join('users', 'employees.id_user', '=', 'users.id_user')->get();
        return view('pages.payroll.update', compact('payroll', 'employees'));
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $edit = [
            "employee_id" => $request->employee_id,
            "basic_salary" => $request->basic_salary,
            "allowance" => $request->allowance,
            "overtime" => $request->overtime,
            "deduction" => $request->deduction,
            "total_salary" => $request->basic_salary + $request->allowance + $request->overtime - $request->deduction,
            "updated_at" => Carbon::now(),
        ];

        $updatePayroll = Payroll::where('id', '=', $id)
                        ->update($edit);

        return redirect('/payroll');
    }

    public function destroy($id)
    {
        try {
            $payroll = Payroll::findOrFail($id);
            $payroll->delete();
        } catch (Exception $e) {

            return response()->json(["error" => true, "message" => $e->getMessage()]);
        }

        return response()->json(["error" => false, "message" => "Sukses Menghapus Data Payroll!"]);
    }
}
