<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Workshift;
use App\Models\Employee;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Menu;
use App\Models\UserActivity;

class EmployeeController extends Controller
{

    public function index()
    {
        $workshift = Workshift::all();
        $user = Auth::user();
        $employee = Employee::where('id_user', $user->id_user)->first();
        if ($employee !== null) {
            $user = Auth::user();
            $employee = Employee::where('id_user', $user->id_user)->first();
            $workshift = Workshift::all();
            return view('pages.employee.update', compact('employee', 'workshift'));
        }
        $menu = Menu::where('menu_name', 'Biodata Pegawai')->first();

        UserActivity::log('Mengakses Route /formemployee', 'success', $menu->menu_id);
        return view('pages.employee.create', compact('workshift'));
    }

    public function allEmployee()
    {
        $employee = Employee::join('users', 'employees.id_user', '=', 'users.id_user')->get();
        $menu = Menu::where('menu_name', 'Semua Pegawai')->first();

        UserActivity::log('Mengakses Route /allemployee', 'success', $menu->menu_id);
        return view('pages.employee.index', compact('employee'));
    }

    public function addEmployee(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();

        $validate = Validator::make($data, [
            'nik' => 'required|unique:employees,nik|size:16',
            'place_of_birth' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'religion' => 'required',
            'address' => 'required',
            'education' => 'required',
            'position' => 'required',
            'workshift_id' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'error' => $validate->errors()->toArray()
            ]);
        }

        $employeeData = Employee::create([
            'employee_id' => Str::uuid(),
            'id_user' => $user->id_user,
            'nik' => $data['nik'],
            'place_of_birth' => $data['place_of_birth'],
            'date_of_birth' => Carbon::parse($data['date_of_birth'])->format('Y-m-d'),
            'gender' => $data['gender'],
            'religion' => $data['religion'],
            'address' => $data['address'],
            'education' => $data['education'],
            'position' => $data['position'],
            'company_id' => 1,
            'workshift_id' => $data['workshift_id'],
        ]);

        return redirect('/employee');
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $edit = [
            "nik" => $request->nik,
            "place_of_birth" => $request->place_of_birth,
            "date_of_birth" => Carbon::parse($request->date_of_birth)->format('Y-m-d'),
            "gender" => $request->gender,
            "religion" => $request->religion,
            "address" => $request->address,
            "education" => $request->education,
            "position" => $request->position,
            "workshift_id" => $request->workshift_id,
            "updated_at" => Carbon::now(),
        ];

        $updateMenu = Employee::where('id_user', '=', $user->id_user)
                        ->update($edit);

        return redirect('/employee');
    }
}
