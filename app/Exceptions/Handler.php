<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use App\Models\ErrorApplication;
use Illuminate\Support\Facades\Auth;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function report(Throwable $exception)
    {
        if ($this->shouldReport($exception)) {
            $trace = $exception->getTrace();
            $moduleName = '';
            $controllerName = '';
            $functionName = '';
            foreach ($trace as $traceStep) {
                if (isset($traceStep['class']) && strpos($traceStep['class'], 'App\\Http\\Controllers\\') !== false) {
                    $controller = str_replace('App\\Http\\Controllers\\', '', $traceStep['class']);
                    $controllerName = str_replace('\\', '.', $controller);
                    $functionName = $traceStep['function'];
                    break;
                } elseif (isset($traceStep['file']) && strpos($traceStep['file'], 'routes/web.php') !== false) {
                    $moduleName = 'Web Routes';
                    break;
                } elseif (isset($traceStep['file']) && strpos($traceStep['file'], 'routes/api.php') !== false) {
                    $moduleName = 'API Routes';
                    break;
                }
            }
            $errorLog = new ErrorApplication();
            $errorLog->id_user = Auth::id();
            $errorLog->error_date = now();
            $errorLog->modules = $moduleName;
            $errorLog->controller = $controllerName;
            $errorLog->function = $functionName;
            $errorLog->error_line = $exception->getLine();
            $errorLog->error_message = $exception->getMessage();
            $errorLog->status = 0;
            $errorLog->param = json_encode(request()->all());
            $errorLog->create_date = now()->format('Y-m-d');
            $errorLog->create_time = now()->format('H:i:s');
            $errorLog->save();
        }

        parent::report($exception);
    }
}
