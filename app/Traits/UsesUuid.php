<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait UsesUuid
{
    protected $primaryKeyName = 'id';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->primaryKeyName = $this->primaryKey ?? $this->primaryKeyName;
    }

    protected static function bootUsesUuid()
    {
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Str::uuid()->toString();
        });
    }

    public function getKeyType()
    {
        return 'string';
    }

    public function getKeyName()
    {
        return $this->primaryKeyName;
    }
}
