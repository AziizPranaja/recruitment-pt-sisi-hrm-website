<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFoto extends Model
{
    use HasFactory;

    protected $table = 'user_photo';

    protected $fillable = [
        'id_user',
        'foto',
        'create_by',
        'update_by',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
