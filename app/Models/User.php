<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, UsesUuid, SoftDeletes;

    protected $primaryKey = 'id_user';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'no_hp',
        'wa',
        'pin',
        'create_by',
        'update_by',
        'name',
        'email',
        'password',
        'id_jenis_user',
        'status_user',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['delete_mark'];
    protected $deletedAt = 'delete_mark';

    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'menu_user', 'id_user', 'menu_id');
    }

    public function foto()
    {
        return $this->hasOne(UserFoto::class, 'id_user');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id_user');
    }

    public function errors()
    {
        return $this->hasMany(Error::class);
    }

    public function payrolls()
    {
        return $this->hasMany(Payroll::class);
    }
}
