<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class MenuLevel extends Model
{
    use HasFactory, UsesUuid;

    protected $primaryKey = 'id_level';
    public $incrementing = false;
}
