<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_user', 'employee_id', 'basic_salary', 'allowance', 'overtime', 'deduction', 'total_salary', 'payroll_date'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
