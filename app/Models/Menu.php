<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use HasFactory, UsesUuid, SoftDeletes;

    protected $primaryKey = 'menu_id';
    public $incrementing = false;

    protected $fillable = [
        'id_level',
        'menu_name',
        'menu_link',
        'menu_icon',
        'parent_id',
        'create_by',
        'update_by',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $deletedAt = 'delete_mark';

    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'menu_user', 'menu_id', 'id_user');
    }
}
