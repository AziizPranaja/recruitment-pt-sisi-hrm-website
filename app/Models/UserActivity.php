<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserActivity extends Model
{
    use HasFactory;

    protected $table = 'user_activity';

    protected $fillable = [
        'id_user',
        'descripsi',
        'status',
        'menu_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public static function log($deskripsi, $status, $menu_id)
    {
        self::create([
            'id_user' => Auth::id(),
            'descripsi' => $deskripsi,
            'status' => $status,
            'menu_id' => $menu_id,
        ]);
    }
}
