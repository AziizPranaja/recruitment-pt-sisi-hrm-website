<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErrorApplication extends Model
{
    use HasFactory;

    protected $table = 'error_application';

    protected $fillable = [
        'id_user',
        'error_date',
        'modules',
        'controller',
        'function',
        'error_line',
        'error_message',
        'status',
        'param',
        'create_date',
        'create_time',
        'update_by',
        'update_date',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
