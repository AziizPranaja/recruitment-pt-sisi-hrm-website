<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $primaryKey = 'employee_id';
    public $incrementing = false;

    protected $guarded = ['employee_id'];

    protected $fillable =  [
        'employee_id',
        'id_user',
        'education',
        'position',
        'place_of_birth',
        'date_of_birth',
        'gender',
        'address',
        'phone',
        'religion',
        'company_id',
        'workshift_id',
        'nik'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
