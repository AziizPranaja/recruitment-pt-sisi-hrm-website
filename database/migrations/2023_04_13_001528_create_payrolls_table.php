<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrolls', function (Blueprint $table) {
            $table->id();
            $table->uuid('id_user');
            $table->uuid('employee_id');
            $table->bigInteger('basic_salary');
            $table->bigInteger('allowance');
            $table->bigInteger('overtime');
            $table->bigInteger('deduction');
            $table->bigInteger('total_salary');
            $table->date('payroll_date');
            $table->timestamps();

            $table->foreign('id_user')->references('id_user')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('employee_id')->references('employee_id')
                ->on('employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
};
