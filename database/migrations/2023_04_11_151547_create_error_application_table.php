<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('error_application', function (Blueprint $table) {
            $table->id();
            $table->uuid('id_user');
            $table->foreign('id_user')->references('id_user')->on('users')->onDelete('cascade');
            $table->dateTime('error_date');
            $table->string('modules');
            $table->string('controller');
            $table->string('function');
            $table->integer('error_line');
            $table->text('error_message');
            $table->integer('status')->default(0);
            $table->text('param')->nullable();
            $table->date('create_date');
            $table->time('create_time');
            $table->softDeletes();
            $table->unsignedBigInteger('update_by')->nullable();
            $table->dateTime('update_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('error_application');
    }
};
