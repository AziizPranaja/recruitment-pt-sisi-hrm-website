<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->uuid('employee_id')->primary();
            $table->uuid('id_user');
            $table->string('nik', 16)->unique();
            $table->string('place_of_birth');
            $table->date('date_of_birth');
            $table->string('gender');
            $table->text('address');
            $table->string('religion');
            $table->string('education');
            $table->string('position');
            $table->unsignedBigInteger('company_id');
            $table->bigInteger('workshift_id');
            $table->timestamps();

            $table->foreign('id_user')->references('id_user')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
