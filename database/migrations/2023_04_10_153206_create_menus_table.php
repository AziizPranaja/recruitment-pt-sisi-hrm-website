<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->uuid('menu_id')->primary();
            $table->uuid('id_level');
            $table->string('menu_name', 300)->unique();
            $table->string('menu_link', 300);
            $table->string('menu_icon', 300);
            $table->string('create_by', 30);
            $table->string('update_by', 30);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('id_level')->references('id_level')
                ->on('menu_levels')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
};
