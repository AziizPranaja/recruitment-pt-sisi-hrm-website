<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('official_duties', function (Blueprint $table) {
            $table->id('official_duty_id');
            $table->uuid('id_user');
            $table->dateTime('duty_from_date');
            $table->dateTime('duty_to_date');
            $table->date('duty_date');
            $table->text('purpose');
            $table->time('time');
            $table->string('place');
            $table->string('abandoned_job');
            $table->string('attachment')->nullable();
            $table->timestamps();

            $table->foreign('id_user')->references('id_user')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('official_duties');
    }
};
