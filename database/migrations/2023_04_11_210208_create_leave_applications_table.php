<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_applications', function (Blueprint $table) {
            $table->id('leave_application_id');
            $table->uuid('id_user');
            $table->unsignedBigInteger('leave_type_id');
            $table->dateTime('application_from_date');
            $table->dateTime('application_to_date');
            $table->date('application_date');
            $table->text('purpose');
            $table->string('abandoned_job');
            $table->timestamps();

            $table->foreign('id_user')->references('id_user')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_applications');
    }
};
