<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_user', function (Blueprint $table) {
            $table->id('no_setting');
            $table->uuid('id_user', 30);
            $table->uuid('menu_id');
            $table->string('create_by', 30)->nullable();
            $table->string('update_by', 30)->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('id_user')->references('id_user')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('menu_id')->references('menu_id')
                ->on('menus')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_user');
    }
};
