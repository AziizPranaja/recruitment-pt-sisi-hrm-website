<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MenuLevelSeeder::class,
            MenuSeeder::class,
            CompanySeeder::class,
            WorkshiftSeeder::class,
            WorkdaySeeder::class,
            LeaveTypeSeeder::class,
            DaySeeder::class,
        ]);
    }
}
