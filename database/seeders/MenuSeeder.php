<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            [
                'menu_id' => Str::uuid(),
                'menu_name' => 'Menu',
                'menu_link' => '/menu',
                'menu_icon' => 'fa fa-navicon',
                'parent_id' => Str::uuid(),
                'id_level' => '06f0f4c5-3931-4dcb-b741-6bf5810a1160',
                'create_by' => 'test',
                'update_by' => 'test',
            ]
        ];

        DB::table('menus')->insert($menus);
    }
}
