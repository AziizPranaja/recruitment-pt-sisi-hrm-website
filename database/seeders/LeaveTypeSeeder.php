<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\LeaveType;

class LeaveTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $leaveTypes = ['Cuti Melahirkan', 'Cuti Berobat', 'Cuti Kematian Ahli Keluarga', 'Cuti Haji/Umroh', 'Cuti Lain - Lain'];

        foreach ($leaveTypes as $leaveType) {
            LeaveType::create([
                'leave_type_name' => $leaveType,
            ]);
        }
    }
}
