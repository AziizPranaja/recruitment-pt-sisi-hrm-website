<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::group(['middleware' => ['auth']], function () {
    //Logout
    Route::get('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

    //Route Menu
    Route::prefix('menu')->group(function () {
        Route::get('/', [App\Http\Controllers\MenuController::class, 'index'])->name('menu.index');
        Route::get('/create-menu', [App\Http\Controllers\MenuController::class, 'create'])->name('menu.create');
        Route::get('/{menu}/add-submenu', [App\Http\Controllers\MenuController::class, 'createSubmenu'])->name('menu.createSubmenu');
        Route::post('/{menu}/add-submenu', [App\Http\Controllers\MenuController::class, 'storeSubmenu'])->name('menu.storeSubmenu');
        Route::post('/create-menu', [App\Http\Controllers\MenuController::class, 'store'])->name('menu.store');
        Route::get('/update-menu/{id}', [App\Http\Controllers\MenuController::class, 'edit'])->name('menu.edit');
        Route::post('/update-menu/{id}', [App\Http\Controllers\MenuController::class, 'update'])->name('menu.update');
        Route::delete('/delete-menu/{id}', [App\Http\Controllers\MenuController::class, 'destroy'])->name('menu.destroy');
    });

    //Route Hak Akses
    Route::prefix('access')->group(function () {
        Route::get('/', [App\Http\Controllers\HakAksesController::class, 'index'])->name('menu.access.index');
        Route::post('/create', [App\Http\Controllers\HakAksesController::class, 'store'])->name('menu.access.add');
        Route::delete('/delete', [App\Http\Controllers\HakAksesController::class, 'destroy'])->name('menu.access.destroy');
    });

    //Route Log Activity
    Route::prefix('activity')->group(function () {
        Route::get('/', [App\Http\Controllers\UserActivityController::class, 'index'])->name('menu.activity.index');
    });

    //Route Log Error Log
    Route::prefix('errorlog')->group(function () {
        Route::get('/', [App\Http\Controllers\ErrorLogController::class, 'index'])->name('menu.errorlog.index');
    });

    //Route Pegawai
    Route::prefix('employee')->group(function () {
        Route::get('/', [App\Http\Controllers\EmployeeController::class, 'index']);
        Route::get('/allemployee', [App\Http\Controllers\EmployeeController::class, 'allEmployee']);
        Route::post('/formemployee', [App\Http\Controllers\EmployeeController::class, 'addEmployee'])->name('formemployee');
        Route::post('/update', [App\Http\Controllers\EmployeeController::class, 'update'])->name('update');
    });

    //Route Absensi
    Route::prefix('absensi')->group(function (){
        Route::get('/', [App\Http\Controllers\AttendanceController::class, 'index']);
        Route::get('/landpage', [App\Http\Controllers\AttendanceController::class, 'absensi']);
        Route::post('/cekin', [App\Http\Controllers\AttendanceController::class, 'checkin']);
        Route::post('/cekout', [App\Http\Controllers\AttendanceController::class, 'checkOut']);
        Route::post('/cuti', [App\Http\Controllers\AttendanceController::class, 'addLeave']);
        Route::post('/duty', [App\Http\Controllers\AttendanceController::class, 'addDuty']);
        Route::get('/page-checkout', [App\Http\Controllers\AttendanceController::class, 'absensiKeluar']);
    });

    //Route Payroll
    Route::prefix('payroll')->group(function (){
        Route::get('/', [App\Http\Controllers\PayrollController::class, 'index'])->name('payroll.index');
        Route::get('/create-payroll', [App\Http\Controllers\PayrollController::class, 'create']);
        Route::post('/', [App\Http\Controllers\PayrollController::class, 'store'])->name('payroll.post');
        Route::delete('/delete-payroll/{id}', [App\Http\Controllers\PayrollController::class, 'destroy'])->name('payroll.destroy');
        Route::get('/update-payroll/{id}', [App\Http\Controllers\PayrollController::class, 'edit'])->name('papyroll.edit');
        Route::post('/update-payroll/{id}', [App\Http\Controllers\PayrollController::class, 'update'])->name('payroll.update');
    });
});
